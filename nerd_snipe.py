#!/usr/bin/env python3
import random
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation


class Car:
    def __init__(self, theta, omega):
        self.theta = theta
        self.omega = omega

christina = Car(0, .5)

N = 50
MAX_SPEED = 1

initial_pos = []
initial_vel = []


#get some initial speeds and positions
for i in range(0, N):
    initial_pos.append(random.random() * 6.28)
    initial_vel.append(random.random() * MAX_SPEED)

# make a list of cars with speeds and positions
cars = []
for i in range(0, N):
   cars.append(Car(initial_pos[i], initial_vel[i]))

t = 0
dt = .01
epsilon = .01
speed_increment = .1
MAX_t = 10

times = []
christina_thetas = []
car0_thetas = []
car_thetas = [[] for i in cars]
while t <= MAX_t:
    times.append(t)
    t += dt
    christina.theta += christina.omega * dt
    christina_thetas.append(christina.theta)
    car0_thetas.append(cars[0].theta)
    for car in cars:

        car_thetas[cars.index(car)].append(car.theta)

        car.theta += car.omega * dt
        car.theta = car.theta % 6.28
        if car.theta - epsilon <= christina.theta <= car.theta + epsilon:
            if car.omega >= christina.omega:
                christina.omega += speed_increment
            elif car.omega <= christina.omega:
                christina.omega -= speed_increment


print("simulation of", N, "cars ran for ", t, "time units")
print("average speed of all other cars: ", sum(initial_vel) / N)
print("christina's final speed: ", christina.omega)



fig, ax = plt.subplots()
ax.set_xlim([-1, 1])
ax.set_ylim([-1,1])
scat = ax.scatter(1, 0, color="red")
scat2 = ax.scatter(1, 0, color="blue")
#t = np.linspace(0, MAX_t)

thetas = []
thetas.append(christina_thetas)
#thetas.append(car0_thetas)
for stuff in car_thetas:
    thetas.append(stuff)
def animate(i):
    scat2.set_offsets((np.cos(thetas[0][i]), np.sin(thetas[0][i])))
    scat.set_offsets([(np.cos(j[i]), np.sin(j[i])) for j in thetas])
    #scat.set_offsets([(np.cos(christina_thetas[i]), np.sin(christina_thetas[i])), (np.cos(car_thetas[i]), np.sin(car_thetas[i]))])
    return scat,

ani = animation.FuncAnimation(fig, animate, repeat=True,
                                    frames=len(times) - 1, interval=50)

# To save the animation using Pillow as a gif
writer = animation.PillowWriter(fps=15, metadata=dict(artist='Me'), bitrate=1800)
ani.save('nerdsnipe.gif', writer=writer)

plt.show()
